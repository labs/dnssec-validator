
#include <sys/types.h>

#ifdef _WIN64
//__MINGW_EXTENSION
typedef unsigned long long _sigset_t;
#else
typedef unsigned long   _sigset_t;
#endif

typedef _sigset_t       sigset_t;

#ifndef sigemptyset
#  define sigemptyset(pset)    (*(pset) = 0)
#endif

#ifndef sigfillset
#  define sigfillset(pset)     (*(pset) = (_sigset_t)-1)
#endif

#ifndef sigaddset
#  define sigaddset(pset, num) (*(pset) |= (1L<<(num)))
#endif

#ifndef sigdelset
#  define sigdelset(pset, num) (*(pset) &= ~(1L<<(num)))
#endif

#ifndef sigismember
#  define sigismember(pset, num) (*(pset) & (1L<<(num)))
#endif
