#!/usr/bin/env sh

SCRIPT_LOCATION=$(dirname $(readlink -f "$0"))
SRC_ROOT="${SCRIPT_LOCATION}/.."

cd "${SRC_ROOT}"

USAGE="$0 [w32|w64|w64+32]"

MINGW_PREFIX=""
MINGW_PREF32=""
MINGW_PREF64=""

I586_MINGW="i586-mingw32msvc" # Unbound 1.5.x requires patching.
I686_MINGW="i686-w64-mingw32"
X86_64_MINGW="x86_64-w64-mingw32"

# Unfortunately mingw32 does not support -m32/-m64 options. Therefore the
# compiler must be called separately.
FAT_TARGET="no"

if [ "x$1" = "x" ]; then
	MINGW_PREFIX="${I586_MINGW}"
	#MINGW_PREFIX=i686-w64-mingw32
	#MINGW_PREFIX=x86_64-w64-mingw32
else
	case "$1" in
	"w32")
		MINGW_PREFIX="${I686_MINGW}"
		;;
	"w64")
		MINGW_PREFIX="${X86_64_MINGW}"
		;;
	"w64+32")
		MINGW_PREF32="${I686_MINGW}"
		MINGW_PREF64="${X86_64_MINGW}"
		FAT_TARGET="yes"
		;;
	*)
		echo "Unknown option '$1'."
		echo "Usage:"
		echo "${USAGE}"
		exit 1
		;;
	esac
fi

if [ "x${FAT_TARGET}" = "xno" ]; then
	"${MINGW_PREFIX}-gcc" -v 2>/dev/null || echo "${MINGW_PREFIX}-gcc not found"
	"${MINGW_PREFIX}-gcc" -v 2>/dev/null || exit 1
elif [ "x${FAT_TARGET}" = "xyes" ]; then
	"${MINGW_PREF32}-gcc" -v 2>/dev/null || echo "${MINGW_PREF32}-gcc not found"
	"${MINGW_PREF32}-gcc" -v 2>/dev/null || exit 1

	"${MINGW_PREF64}-gcc" -v 2>/dev/null || echo "${MINGW_PREF64}-gcc not found"
	"${MINGW_PREF64}-gcc" -v 2>/dev/null || exit 1
fi

# NPAPI extension was built directly in Visual Studio.

if [ "x${FAT_TARGET}" = "xno" ]; then

	CONF_OPTS=""
	CONF_OPTS="${CONF_OPTS} --enable-static-linking"
	#CONF_OPTS="${CONF_OPTS} --enable-npapi-extensions"
	# Don't force ABI, let the script decide.
	#CONF_OPTS="${CONF_OPTS} --with-force-abi=x86"
	CONF_OPTS="${CONF_OPTS} --host=${MINGW_PREFIX} --target=${MINGW_PREFIX}"

	./configure ${CONF_OPTS}
	rm add-on/*.xpi
	make clean
	make

elif [ "x${FAT_TARGET}" = "xyes" ]; then

	XPI64=win64.xpi
	XPI32=win32.xpi
	XPIFAT=winfat.xpi

	# Create 64-bit first.
	CONF_OPTS=""
	CONF_OPTS="${CONF_OPTS} --enable-static-linking"
	#CONF_OPTS="${CONF_OPTS} --enable-npapi-extensions"
	# Don't force ABI, let the script decide.
	#CONF_OPTS="${CONF_OPTS} --with-force-abi=x86_64"
	CONF_OPTS="${CONF_OPTS} --host=${MINGW_PREF64} --target=${MINGW_PREF64}"

	./configure ${CONF_OPTS}
	rm add-on/*.xpi
	make clean
	make

	XPI64=$(ls add-on/*.xpi | sed -e 's/.*\///g')
	mv add-on/${XPI64} ./${XPI64}

	# Follow with 32-bits.
	CONF_OPTS=""
	CONF_OPTS="${CONF_OPTS} --enable-static-linking"
	#CONF_OPTS="${CONF_OPTS} --enable-npapi-extensions"
	# Don't force ABI, let the script decide.
	#CONF_OPTS="${CONF_OPTS} --with-force-abi=x86"
	CONF_OPTS="${CONF_OPTS} --host=${MINGW_PREF32} --target=${MINGW_PREF32}"

	./configure ${CONF_OPTS}
	rm add-on/*.xpi
	make clean
	make

	XPI32=$(ls add-on/*.xpi | sed -e 's/.*\///g')
	mv add-on/${XPI32} ./${XPI32}

	# Inflate fat xpi.
	XPIFAT=$(echo "${XPI32}" | sed -e 's/x86\(_64\)\?/fat/g')
	cp "${XPI32}" "${XPIFAT}"

	rm -rf "platform"
	unzip "${XPI64}" \
		"platform/libDANEcore-WINNT-x86_64.dll" \
		"platform/libDNSSECcore-WINNT-x86_64.dll"
	zip -g "${XPIFAT}" \
		"platform/libDANEcore-WINNT-x86_64.dll" \
		"platform/libDNSSECcore-WINNT-x86_64.dll"
	rm -rf "platform"

fi

#./scripts/install_chrome_add_payload.sh -C google-chrome
