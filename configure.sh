#!/usr/bin/env sh

USAGE="Usage: configure.sh [debug|help]"

CONF_OPTS=""
CONF_OPTS="${CONF_OPTS} --enable-static-linking"
CONF_OPTS="${CONF_OPTS} --with-force-abi=fat"

#CONF_OPTS="${CONF_OPTS} --host=i586-mingw32msvc"
#CONF_OPTS="${CONF_OPTS} --host=i686-w64-mingw32"
#CONF_OPTS="${CONF_OPTS} --host=x86_64-w64-mingw32"
#CONF_OPTS="${CONF_OPTS} --with-win32-host=i686-w64-mingw32"
#CONF_OPTS="${CONF_OPTS} --with-win64-host=x86_64-w64-mingw32"

if [ "x$1" != "x" ]; then
	for param in $@; do
		case ${param} in
		debug)
			CONF_OPTS="${CONF_OPTS} --enable-debug"
			;;
		help)
			echo ${USAGE}
			exit
			;;
		*)
			echo ${USAGE} >&2
			exit 1
			;;
		esac
	done
fi

CMD="./configure ${CONF_OPTS}"

echo ${CMD}
${CMD}
